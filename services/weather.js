import axios from "axios";
const OPEN_WEATHER_KEY = "a5a47c18197737e8eeca634cd6acb581";

// const forecastEndpoint = params=> `https://api.weatherapi.com/v1/forecast.json?key=${apiKey}&q=${params.cityName}&days=${params.days}`;
// const locationsEndpoint = params=> `https://api.weatherapi.com/v1/search.json?key=${apiKey}&q=${params.cityName}`;
const locationsEndpoint = params=> `https://search.reservamos.mx/api/v2/places?q=${params.cityName}`;
const forecastEndpoint = params=> `https://api.openweathermap.org/data/2.5/onecall?lat=${params.lat}&lon=${params.long}&exclude=minutely,hourly,alerts&appid=${OPEN_WEATHER_KEY}&units=metric`;

const apiCall = async (endpoint)=>{
    const options = {
        method: 'GET',
        url: endpoint,
    };

      try{
        const response = await axios.request(options);
        return response.data;
      }catch(error){
        console.log('error: ',error);
        return {};
    }
}

export const  fetchWeatherForecast = params=>{
    let forecastUrl = forecastEndpoint(params);
    return apiCall(forecastUrl);
}

export const fetchLocations = params=>{
    let locationsUrl = locationsEndpoint(params);
    return apiCall(locationsUrl);
}
