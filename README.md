## Requeriments
Andorid studio installed with sdk and emulator or cellphone

## Get Started

install the latest version of expo

### `npm install -g expo-cli`

## Then

install dev dependencies

### `npm install` or `yarn install`

## Then

Start the app

### `npx expo start`

## Then

Run The app

After pressing npm start, a menu will appear in which you can choose where to run the application. In case you decide to use Android you must press the letter "a" then a list of available devices will be displayed, either your Android Studio emulator which must be runnin or your own cell phone having activated it in developer mode first and USB debugging
activated also you need connect it via USB(I reccomend this way) or download the Expo Go App on the play store.

## Then

Enjoy the weather forecast
