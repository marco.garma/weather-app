import { View, Text, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { MagnifyingGlassIcon, XMarkIcon } from 'react-native-heroicons/outline'
import { CalendarDaysIcon, MapPinIcon } from 'react-native-heroicons/solid'
import { debounce } from "lodash";
import { theme } from '../theme';
import { fetchLocations, fetchWeatherForecast } from '../services/weather';
import * as Progress from 'react-native-progress';
import { StatusBar } from 'expo-status-bar';
import { weatherImages } from '../constants';
import { getData, storeData } from '../utils/asyncStorage';

export default function HomeScreen() {
  const [showSearch, toggleSearch] = useState(false);
  const [locations, setLocations] = useState([]);
  const [filteredLocations, setFilteredLocations] = useState(locations);
  const [loading, setLoading] = useState(true);
  const [weather, setWeather] = useState({});
  const [city, setCity] = useState({});


  const handleSearch = search=>{
    setLocations([]);
    setFilteredLocations([]);
    if(search && search.length>2){
      fetchLocations({cityName: search}).then(data=>{
        const filteredLocations = data.filter(locations => locations?.result_type.includes("city"));
        setLocations(filteredLocations);
      })
    }
  };

  const handleLocation = loc=>{
    setLoading(true);
    toggleSearch(false);
    setLocations([]);
    setFilteredLocations([]);
    setCity(loc);
    fetchWeatherForecast({
      lat: loc.lat,
      long: loc.long
    }).then(data=>{
      setLoading(false);
      setWeather(data);
      storeData('city',loc.city_name);
      storeData('country',loc.country);
      storeData('lat',loc.lat);
      storeData('long',loc.long);
    })
  }

  useEffect(()=>{
    fetchMyWeatherData();
  },[]);

  const fetchMyWeatherData = async ()=>{
    let cityName = await getData('city');
    let countryName = await getData('country');
    let myLat = await getData('lat');
    let myLong = await getData('long');
    if(cityName && countryName){
      let array = {city_name: cityName, country: countryName};
      setCity(array);
    }
    fetchWeatherForecast({
      lat: myLat,
      long: myLong
    }).then(data=>{
      setWeather(data);
      setLoading(false);

    })
    
  }

  const handleTextDebounce = useCallback(debounce(handleSearch, 1200), []);

  const windSpeed = weather?.current?.wind_speed * 3.6;
  const hourSunrise = new Date(weather?.current?.sunrise * 1000);
  const hours = hourSunrise.getHours();
  const minutes = hourSunrise.getMinutes();
  const seconds = hourSunrise.getSeconds();

  return (
    <View className="flex-1 relative">
      <StatusBar style="light" />
      <Image 
        blurRadius={70} 
        source={require('../assets/images/bg.png')} 
        className="absolute w-full h-full" />
        {
          loading? (
            <View className="flex-1 flex-row justify-center items-center">
              <Progress.CircleSnail thickness={10} size={140} color="#0bb3b2" />
            </View>
          ):(
            <SafeAreaView className="flex flex-1">
              <View style={{height: '7%'}} className="mx-4 relative z-50">
                <View 
                  className="flex-row justify-end items-center rounded-full" 
                  style={{backgroundColor: showSearch? theme.bgWhite(0.2): 'transparent'}}>
                  
                    {
                      showSearch? (
                        <TextInput 
                          onChangeText={handleTextDebounce} 
                          placeholder="Search City" 
                          placeholderTextColor={'lightgray'} 
                          className="pl-6 h-10 pb-1 flex-1 text-base text-white" 
                        />
                      ):null
                    }
                    <TouchableOpacity 
                      onPress={()=> toggleSearch(!showSearch)} 
                      className="rounded-full p-3 m-1" 
                      style={{backgroundColor: theme.bgWhite(0.3)}}>
                      {
                        showSearch? (
                          <XMarkIcon size="25" color="white" />
                        ):(
                          <MagnifyingGlassIcon size="25" color="white" />
                        )
                      }
                      
                  </TouchableOpacity>
                </View>
                {
                  locations.length>0 && showSearch?(
                    <View className="absolute w-full bg-gray-300 top-16 rounded-3xl ">
                    <ScrollView >
                      {
                        locations.map((loc, index)=>{
                          let showBorder = index+1 != locations.length;
                          let borderClass = showBorder? ' border-b-2 border-b-gray-400':'';
                          return (
                            <View>
                            <TouchableOpacity 
                              key={index}
                              onPress={()=> handleLocation(loc)} 
                              className={"flex-row items-center border-0 p-3 px-4 mb-1 "+borderClass}>
                                <MapPinIcon size="20" color="gray" />
                                <Text className="text-black text-lg ml-2">{loc?.display}, {loc?.country}</Text>
                            </TouchableOpacity>
                            </View>
                          )
                        })
                      }
                      </ScrollView>
                    </View>
                  ):null
                }
                
              </View>

              <View className="mx-4 flex justify-around flex-1 mb-2">
                <Text className="text-white text-center text-2xl font-bold">
                  {city?.city_name}, 
                  <Text className="text-lg font-semibold text-gray-300">{city?.country}</Text>
                </Text>
                <View className="flex-row justify-center">
                  <Image 
                    source={weatherImages[weather?.current?.weather?.[0]?.main || 'other']} 
                    className="w-52 h-52" />
                  
                </View>
                <View className="space-y-2">
                    <Text className="text-center font-bold text-white text-6xl ml-5">
                      {weather?.current?.temp}&#176;
                    </Text>
                    <Text className="text-center text-white text-xl tracking-widest">
                      {weather?.current?.weather?.[0]?.main}
                    </Text>
                </View>

                <View className="flex-row justify-between mx-4">
                  <View className="flex-row space-x-2 items-center">
                    <Image source={require('../assets/icons/wind.png')} className="w-6 h-6" />
                    <Text className="text-white font-semibold text-base" maxLe>{windSpeed} km/h</Text>
                  </View>
                  <View className="flex-row space-x-2 items-center">
                    <Image source={require('../assets/icons/drop.png')} className="w-6 h-6" />
                    <Text className="text-white font-semibold text-base">{weather?.current?.humidity}%</Text>
                  </View>
                  <View className="flex-row space-x-2 items-center">
                    <Image source={require('../assets/icons/sun.png')} className="w-6 h-6" />
                    <Text className="text-white font-semibold text-base">
                      { `${hours}:${minutes}:${seconds}` }
                    </Text>
                  </View>
                  
                </View>
              </View>

              <View className="mb-2 space-y-3">
                <View className="flex-row items-center mx-5 space-x-2">
                  <CalendarDaysIcon size="22" color="white" />
                  <Text className="text-white text-base">Daily forecast</Text>
                </View>
                <ScrollView   
                  horizontal
                  contentContainerStyle={{paddingHorizontal: 15}}
                  showsHorizontalScrollIndicator={false}
                >
                  {
                    weather?.daily?.map((item, index)=>{
                      const date = new Date(item.dt * 1000);
                      let dayName = date.toString();
                      dayName = dayName.split(' ')[0];
                      let day = date.getDate();
                      return (
                        <View 
                          key={index} 
                          className="flex justify-center items-center w-24 rounded-3xl py-3 space-y-1 mr-4" 
                          style={{backgroundColor: theme.bgWhite(0.15)}}
                        >
                          <Text className="text-white text-4xl font-semibold">{dayName}</Text>
                          <Text className="text-white text-xl">{day}</Text>
                          <Text className="text-white text-lg font-semibold">
                            {'Min ' + item?.temp?.min}&#176;
                          </Text>
                          <Text className="text-white text-lg font-semibold">
                            {'Max ' + item?.temp?.max}&#176;
                          </Text>
                        </View>
                      )
                    })
                  }
                  
                </ScrollView>
              </View>
              
            
            </SafeAreaView>
          )
        }
      
    </View>
  )
}
