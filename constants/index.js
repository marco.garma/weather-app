export const weatherImages = {
    'Thunderstorm': require('../assets/images/heavyrain.png'),
    'Drizzle': require('../assets/images/moderaterain.png'),
    'Rain': require('../assets/images/heavyrain.png'),
    'Snow': require('../assets/images/snow.png'),
    'Clear': require('../assets/images/sun.png'),
    'Clouds': require('../assets/images/cloud.png'),
    'Mist': require('../assets/images/mist.png'),
    'Smoke': require('../assets/images/smoke.png'),
    'Haze': require('../assets/images/mist.png'),
    'Dust': require('../assets/images/dust.png'),
    'Fog': require('../assets/images/mist.png'),
    'Sand': require('../assets/images/dust.png'),
    'Ash': require('../assets/images/ash.png'),
    'Squall': require('../assets/images/heavyrain.png'),
    'Tornado': require('../assets/images/tornado.png')
}